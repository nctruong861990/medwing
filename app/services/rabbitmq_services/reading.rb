module RabbitmqServices

  class Reading < Base

    # Options for queues
    OPTIONS = {
      channel: 'medwing',
      exchange: 'medwing.readings',
      delayed_exchange: {
        publisher: {
          exchange: 'delayed.exchange',
          exchange_options: {
            type: 'x-delayed-message',
            arguments: { 'x-delayed-type' => 'direct' },
            durable: true,
            auto_delete: false
          },
        },
        headers: { 'x-delay': 1000 }
      },
      queues: {
        create: 'readings.create',
        delete: 'readings.delete'
      }
    }

    class << self

      # Publishing temporary data to queue for consumers to process.
      # Saving data in Redis for quick response.
      def publish(reading_attributes)
        last_id = publish_to_queue(reading_attributes)
        save_in_redis(last_id, reading_attributes)
        last_id
      end

      # Delayed deleting reading in Redis due to consistent data response.
      def delayed_delete_reading(id)
        Sneakers::Publisher.new(OPTIONS[:delayed_exchange][:publisher])
          .publish(id.to_s, headers: OPTIONS[:delayed_exchange][:headers], routing_key: OPTIONS[:queues][:delete]);
      end

      # Looking for reading data from both Redis and Database.
      def get_thermostat_by_reading_id(id)
        result = grab_result(id)
        block_given? ? yield(result) : RedisServices::Readings::DataType.string_to_json(result)
      end

      private

        def grab_result(id)
          # Benchmark.bm do |x|
          #   x.report do
          # byebug
          RedisServices::ReadingPool.find_by(reading_id: id) || ::Reading.find_by(id: id).to_json
          #   end
          # end
          # result.to_json if result.present?
        end

        # Publishing to queue for consumers to process.
        # Return id which also the newest id - saving it in redis as well to calculate next id.
        def publish_to_queue(reading_attributes)
          id = RedisServices::ReadingPool.nextId
          $post_queue.publish(
              reading_attributes.merge(id: id).to_json, routing_key: OPTIONS[:queues][:create]
          )
          RedisServices::ReadingPool.save_last_id(id)
          id
        end

        # Saving in redis for quick response
        def save_in_redis(id, reading_attributes)
          RedisServices::ReadingPool.create(id, reading_attributes)
        end
    end
  end
end