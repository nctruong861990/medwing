module RedisServices::Readings
  module Average
    def set_statistic(thermostat_id, val)
      Redis.current.set(statistic_key(thermostat_id), val)
    end

    def statistic(thermostat_id)
      Redis.current.get(statistic_key(thermostat_id))
    end

    private

    def statistic_key(thermostat_id)
      "#{options[:statistic]}-stats#{thermostat_id}"
    end
  end
end