module RedisServices::Readings
  module IdStorage
    def nextId
      (Redis.current.get(options[:lastId]) || (::Reading.maximum(:id) || 0)).to_i + 1
    end

    def save_last_id(reading_id)
      Redis.current.set(options[:lastId], reading_id)
    end

    def last_id
      Redis.current.get(options[:lastId])
    end

    def remove(reading_id)
      Redis.current.keys(result_key(reading_id)).each { |key| Redis.current.del(key) }
    end
  end
end