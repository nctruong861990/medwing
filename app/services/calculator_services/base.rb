module CalculatorServices
  class Base
    attr_reader :attributes, :opts

    def initialize(attributes, opts = {})
      raise 'Lack of thermostat' unless opts[:thermostat_id].present?
      @attributes = attributes
      @opts = opts
    end

    private

    # Make sure don't forget to retrieve from database.
    def from_db
      raise NotImplementedError
    end

    # Make sure don't forget to retrieve from jobs enqueued.
    def from_pending_jobs
      raise NotImplementedError
    end

    # verifying thermostat.
    def thermostat_exists?(thermostat_id)
      if opts[:thermostat_id] && Thermostat.find_by(id: thermostat_id).nil?
        Rails.logger.debug { "Thermostat id##{thermostat_id} not found" }
      end
    end
  end
end