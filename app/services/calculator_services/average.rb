module CalculatorServices
  class Average < Base
    # Calculates the average from both Redis and Database.
    # Returns json data type including attributes provided.
    def get
      avg = {}
      db_avg = from_db
      pending_avg = from_pending_jobs

      attributes.each do |attr|
        if db_avg[attr] && pending_avg[attr]
          avg[attr] = (db_avg[attr] + pending_avg[attr]) / 2
        else
          avg[attr] = db_avg[attr] || pending_avg[attr]
        end
      end
      avg
    end

    private

    def from_db
      if opts[:thermostat_id] && Thermostat.find_by(id: opts[:thermostat_id]).nil?
        Rails.logger.debug { "Thermostat id##{opts[:thermostat_id]} not found" }
      end
      ReadingQueries::Average.get(attributes,
                                  opts[:thermostat_id] ? "thermostat_id = #{opts[:thermostat_id]}" : '')
    end

    def from_pending_jobs
      avg = {}
      readings = RedisServices::ReadingPool.where(thermostat_id: opts[:thermostat_id])

      readings.replace(
        readings.collect do |reading|
          reading_json = RedisServices::Readings::DataType.string_to_json(reading)
          reading_json unless reading_json['invalid']
        end.compact
      )
      count = readings&.size
      unless readings.blank?
        attributes.each do |attr|
          sum = 0
          readings.each { |reading| sum += reading[attr]&.to_i || 0 }
          avg[attr] = sum / count
        end
      end
      avg
    end
  end
end