module CalculatorServices

  class MinMax < Base

    # Getting minimum values of attributes provided.
    def min
      @min = true
      get
    end

    # Getting maximum values of attributes provided.
    def max
      @min = false
      get
    end

    private

    # Calculating min, max from both Redis and Database.
    def get
      collection = {}
      db_value = from_db
      pending_value = from_pending_jobs

      attributes.each do |attr|
        if db_value[attr] && pending_value[attr]
          collection[attr] = final_value(db_value, pending_value).call(attr)
        else
          collection[attr] = db_value[attr] || pending_value[attr]
        end
      end
      collection
    end

    def from_db
      thermostat_exists?(opts[:thermostat_id])
      if @min
        ReadingQueries::MinMax.min(attributes, opts[:thermostat_id])
      else
        ReadingQueries::MinMax.max(attributes, opts[:thermostat_id])
      end
    end

    def from_pending_jobs
      collection = {}
      readings = RedisServices::ReadingPool.where(thermostat_id: opts[:thermostat_id])
      readings.replace(string_to_json_collection(readings))

      unless readings.blank?
        attributes.each do |attr|
          collection[attr] = min_max(readings).call(attr)
        end
      end
      collection
    end

    def string_to_json_collection(readings)
      readings.collect { |reading| RedisServices::Readings::DataType.string_to_json(reading) }
    end

    def min_max(readings)
      if @min
        value = -> (attr) { readings.collect { |reading| reading[attr]&.to_i if reading[attr].present? }.compact.min }
      else
        value = -> (attr) { readings.collect { |reading| reading[attr]&.to_i if reading[attr].present? }.compact.max }
      end
      value
    end

    def final_value(db_value, pending_value)
      if @min
        value = -> (attr) { db_value[attr] < pending_value[attr] ? db_value[attr] : pending_value[attr] }
      else
        value = -> (attr) { db_value[attr] > pending_value[attr] ? db_value[attr] : pending_value[attr] }
      end
      value
    end
  end

end