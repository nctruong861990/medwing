module CalculatorServices
  class ReadingService < Base

    # Returns statistic data including average, minimum and maximum of attributes provided.
    def statistic
      {
         'avg': average,
         'min': minimum,
         'max': maximum
      }.to_json
    end

    private

    def average
      CalculatorServices::Average.new(attributes, opts).get
    end

    def minimum
      CalculatorServices::MinMax.new(attributes, opts).min
    end

    def maximum
      CalculatorServices::MinMax.new(attributes, opts).max
    end
  end
end