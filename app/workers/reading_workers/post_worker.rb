module ReadingWorkers
  class PostWorker
    include Sneakers::Worker

    from_queue RabbitmqServices::Reading::OPTIONS[:queues][:create],
               exchange: RabbitmqServices::Reading::OPTIONS[:exchange], durable: true,
               'x-queue-mode': 'lazy'

    def work(options)
      readings_json = JSON.parse(options)
      reading = Reading.create(readings_json)

      unless reading.blank?
        mark_invalid_reading(reading.id)
        RabbitmqServices::Reading.delayed_delete_reading(reading.id)
        proceed_statistic(reading.thermostat_id)
        ack!
      end
    end

    private

    def proceed_statistic(thermostat_id)
      result = CalculatorServices::ReadingService.new(['temperature', 'humidity', 'battery_charge'], { thermostat_id: thermostat_id }).statistic
      RedisServices::ReadingPool.set_statistic(thermostat_id, result)
    end

    def mark_invalid_reading(reading_id)
      reading = RedisServices::ReadingPool.find_by(reading_id: reading_id)
      reading = RedisServices::Readings::DataType.string_to_json(reading)
      reading[:invalid] = true

      reading = JSON.parse(reading.to_json, :symbolize_names => true)
      RedisServices::ReadingPool.create(reading_id, reading)
    end
  end
end