module ReadingQueries
  class Base
    private

    def self.result(exec_result)
      result = {}
      exec_result&.columns.each_with_index do |col,idx|
        result[col] = exec_result&.rows[0][idx]
      end
      result
    end
  end
end