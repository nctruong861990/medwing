module ReadingQueries
  class MinMax < Base
    class << self
      def min(attributes, thermostat_id)
        get(attributes, thermostat_id)
      end

      def max(attributes, thermostat_id)
        get(attributes, thermostat_id, func = 'Max')
      end

      private

      def get(attributes, thermostat_id, func = 'Min')
        max_query = attributes.collect { |attribute| "#{func}(#{attribute}) as #{attribute}"}
        sql = "SELECT #{max_query.join(',')} FROM readings WHERE thermostat_id = #{thermostat_id}"
        result(ActiveRecord::Base.connection.exec_query(sql))
      end
    end
  end
end