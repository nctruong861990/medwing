module ReadingQueries
  class Average < Base
    def self.get(attributes, after_where_clause = '')
      sql = ::UtilQueries::AverageQueryBuilder.new(attributes, 'readings', after_where_clause).get
      result(ActiveRecord::Base.connection.exec_query(sql))
    end
  end
end