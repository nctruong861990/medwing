require 'rails_helper'

RSpec.describe ReadingWorkers::PostWorker, type: :worker do
  include_context :reading_params
  include_context :testing_data

  let(:post_worker) { ReadingWorkers::PostWorker.new }

  before(:each) do
    create_sample_data(thermostat_quantity: 20, reading_quantity: 100)
  end

  describe 'valid' do
    include_examples :expected_reading_order

    it 'saves successfully records to db' do
      Sneakers::Testing.messages_by_queue[RabbitmqServices::Reading::OPTIONS[:queues][:create]].each_with_index do |message, idx|
        post_worker.work(message)
        expect(Reading.count).to eq(idx + 1)
      end
    end

    it 'enqueues successfully records to delete later' do
      Sneakers::Testing.messages_by_queue["readings.create"].each_with_index do |message, idx|
        post_worker.work(message)
        expect(Sneakers::Testing.messages_by_queue[RabbitmqServices::Reading::OPTIONS[:queues][:delete]].count).to eq(idx + 1)
      end
    end

    it 'returns :ack' do
      Sneakers::Testing.messages_by_queue["readings.create"].each do |message|
        expect(post_worker.work(message)).to eq(:ack)
      end
    end
  end

  describe 'invalid' do
    it 'throws exception' do
      message = { id: 1_000_000_000 }
      expect { post_worker.work(RedisServices::Readings::DataType.json_to_string message.to_json) }.to raise_exception("Thermostat not found")
    end
  end
end