require 'rails_helper'

RSpec.describe StatsController, type: :controller do
  let!(:thermostat) { create(:thermostat) }
  let(:params) { { thermostat_id: thermostat.id } }
  let(:post_worker) { ReadingWorkers::PostWorker.new }

  include_context :testing_data

  describe '#GET' do
    before(:each) do
      create_data_for_average(thermostat)
      post_messages do |message, idx|
        post_worker.work(message)
      end
      get :statistic, params: params
      @result = JSON.parse(response.body)
    end

    include_examples :expect_average
    include_examples :expect_minimum
    include_examples :expect_maximum
  end
end