module Helpers
  module MessageQueue
    def post_messages
      if block_given?
        Sneakers::Testing.messages_by_queue[RabbitmqServices::Reading::OPTIONS[:queues][:create]].each_with_index do |message, idx|
          yield(message, idx)
        end
      else
        Sneakers::Testing.messages_by_queue[RabbitmqServices::Reading::OPTIONS[:queues][:create]]
      end
    end

    def delete_messages
      if block_given?
        Sneakers::Testing.messages_by_queue[RabbitmqServices::Reading::OPTIONS[:queues][:delete]].each_with_index do |message, idx|
          yield(message, idx)
        end
      else
        Sneakers::Testing.messages_by_queue[RabbitmqServices::Reading::OPTIONS[:queues][:delete]]
      end
    end
  end
end