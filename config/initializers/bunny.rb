def init_bunny
  $bunny_conn ||= Bunny.new
  $bunny_conn.start
  publish_channel ||= $bunny_conn.create_channel
  $post_queue = publish_channel.queue('readings.create', durable: true)
end

unless Rails.env.test?
  begin
    init_bunny
  rescue
    puts "You have not started Rabittmq yet."
    puts "Do you want to proceed without connecting to rabbitmq (y/n)?"
    answer = STDIN.gets.chomp
    exit! if answer.downcase == 'n'
  end
end